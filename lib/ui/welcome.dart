import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Welcome extends StatefulWidget {
  final Function executeOnValid;
  final Map<String, String> inputData;

  Welcome({Key key, this.executeOnValid, this.inputData}) : super(key: key);

  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  bool isFormValid = false;
  final TextEditingController _emailController = new TextEditingController();

  @override
  void initState() {
    _emailController.text = widget.inputData['email'];
    isFormValid = isValidEmail(_emailController.text);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 32),
      child: Column(
        children: [
          Expanded(
            child: Wrap(
              runSpacing: 16,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: Text(
                    'Welcome to\nGIN Finans',
                    style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
                  ),
                ),
                Text(
                  'Welcome to The Bank of The Future. Manage and track your accounts on the go.',
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                ),
                Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Container(
                    child: TextField(
                      controller: _emailController,
                      onChanged: (v) {
                        setState(() {
                          widget.inputData['email'] = v;
                          isFormValid = isValidEmail(v);
                        });
                      },
                      keyboardType: TextInputType.emailAddress,
                      textAlignVertical: TextAlignVertical.center,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        prefixIcon: Icon(
                          Icons.email_outlined,
                          color: Colors.grey,
                        ),
                        hintText: 'Email',
                      ),
                    ),
                    decoration: BoxDecoration(
                      color: Colors.grey.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    margin: EdgeInsets.all(16),
                  ),
                  color: Colors.white,
                  elevation: 2,
                )
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                child: RaisedButton(
                  padding: EdgeInsets.all(18),
                  onPressed: () {
                    if(isFormValid){
                      widget.executeOnValid();
                    }
                  },
                  child: Text(
                    'Next',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 16),
                  ),
                  color:
                      isFormValid ? Colors.blueAccent : Colors.blueAccent.withOpacity(0.7),
                ),
              )
            ],
          )
        ],
      ),
      padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
    );
  }

  bool isValidEmail(String em) {
    String p = r'^\D.+@.+\.[a-z]+';
    RegExp regExp = new RegExp(p);
    return regExp.hasMatch(em);
  }
}
