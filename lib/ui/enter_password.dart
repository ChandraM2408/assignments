import 'package:flutter/material.dart';

class EnterPassword extends StatefulWidget {
  final Function executeOnValid;
  final Map<String, String> inputData;

  EnterPassword({Key key, this.executeOnValid, this.inputData})
      : super(key: key);

  @override
  _EnterPasswordState createState() => _EnterPasswordState();
}

class _EnterPasswordState extends State<EnterPassword> {
  bool isFormValid = false;
  final TextEditingController _passwordController = new TextEditingController();

  @override
  void initState() {
    _passwordController.text = widget.inputData['password'];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 32),
      child: Column(
        children: [
          Expanded(
            child: Wrap(
              runSpacing: 16,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: Text(
                    'Create Password',
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
                Text(
                  'Password will be used to login to account.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
                Container(
                  child: TextField(
                    controller: _passwordController,
                    obscureText: true,
                    onChanged: (v) {
                      setState(() {
                        widget.inputData['password'] = v;
                      });
                    },
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Password',
                        contentPadding: EdgeInsets.all(18)),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                Row(
                  children: [
                    Text(
                      'Complexity: ',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    Text(
                      'Very Weak',
                      style: TextStyle(
                          color: Colors.orange,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                Container(
                  height: 100,
                  child: Row(
                    children: buildPasswordStrengthTracker(
                        widget.inputData['password']),
                  ),
                )
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                child: RaisedButton(
                  padding: EdgeInsets.all(18),
                  onPressed: isFormValid
                      ? () {
                          widget.executeOnValid();
                        }
                      : null,
                  child: Text(
                    'Next',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 16),
                  ),
                  color: isFormValid
                      ? Colors.blueAccent
                      : Colors.blueAccent.withOpacity(0.7),
                ),
              )
            ],
          )
        ],
      ),
      padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
    );
  }

  List<Widget> buildPasswordStrengthTracker(String password) {
    List<Widget> widgets = [];
    var criteria1 = password != null && password.contains(RegExp(r'[a-z]'));
    var criteria2 = password != null && password.contains(RegExp(r'[A-Z]'));
    var criteria3 = password != null && password.contains(RegExp(r'[0-9]'));
    var criteria4 = password.contains(RegExp(r'[!@#$%^&*(),.?":{}|<>+]'));
    widgets.add(passwordStrengthWidget('a', 'Lowercase', criteria1));
    widgets.add(passwordStrengthWidget('A', 'Uppercase', criteria2));
    widgets.add(passwordStrengthWidget('123', 'Number', criteria3));
    widgets.add(passwordStrengthWidget('9+', 'Characters', criteria4));
    setState(() {
      isFormValid = criteria1 && criteria2 && criteria3 && criteria4;
    });
    return widgets;
  }

  Widget passwordStrengthWidget(
      String inputType, String inputTypeDescription, bool valid) {
    return Expanded(
      child: Column(
        children: [
          Stack(
            children: [
              Opacity(
                opacity: valid ? 1 : 0,
                child: Center(
                  child: Icon(Icons.check_circle_rounded, color: Colors.green),
                ),
              ),
              Opacity(
                opacity: valid ? 0 : 1,
                child: Center(
                  child: Text(
                    inputType,
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 8),
            child: Text(
              inputTypeDescription,
              style: TextStyle(fontSize: 12, color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
