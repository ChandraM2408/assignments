import 'package:flutter/material.dart';

import 'widgets/drop_down.dart';

class ScheduleCall extends StatefulWidget {
  final Function executeOnValid;
  final Map<String, String> inputData;

  ScheduleCall({Key key, this.executeOnValid, this.inputData})
      : super(key: key);

  @override
  _ScheduleCallState createState() => _ScheduleCallState();
}

class _ScheduleCallState extends State<ScheduleCall>
    with TickerProviderStateMixin {
  List<String> dateMenu = [
    'Choose Date',
  ];
  List<String> timeMenu = [
    'Choose Time',
  ];
  AnimationController animationController;
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay.now();

  @override
  void initState() {
    if(!dateMenu.contains(widget.inputData['date'])){
      dateMenu.add(widget.inputData['date']);
    }
    if(!timeMenu.contains(widget.inputData['time'])){
      timeMenu.add(widget.inputData['time']);
    }
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    )
      ..forward()
      ..repeat(reverse: true);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: AnimatedBuilder(
              animation: animationController,
              builder: (context, child) {
                return Container(
                  decoration: ShapeDecoration(
                    color: Colors.white.withOpacity(0.5),
                    shape: CircleBorder(),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(4.0 * animationController.value),
                    child: child,
                  ),
                );
              },
              child: Container(
                decoration: ShapeDecoration(
                  color: Colors.white,
                  shape: CircleBorder(),
                ),
                child: IconButton(
                  onPressed: () {},
                  color: Colors.blue,
                  icon: Icon(Icons.calendar_today, size: 24),
                ),
              ),
            ),
            height: 48,
          ),
          Expanded(
            child: Wrap(
              runSpacing: 16,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: Text(
                    'Schedule Video Call',
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
                Text(
                  'Choose the date and time that you preferred, we will send a link via email to make a video call on the scheduled date and time.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
                GestureDetector(
                  child: DropDown(
                    disableClick: true,
                    label: 'Date',
                    items: dateMenu,
                    selectedValue: widget.inputData['date'],
                    onSelected: (value) {
                      setState(() {
                        widget.inputData['date'] = value;
                      });
                      validateForm();
                    },
                  ),
                  onTap: () {
                    _selectDate(context);
                  },
                ),
                GestureDetector(
                  child: DropDown(
                    disableClick: true,
                    label: 'Time',
                    items: timeMenu,
                    selectedValue: widget.inputData['time'],
                    onSelected: (value) {
                      setState(() {
                        widget.inputData['time'] = value;
                      });
                      validateForm();
                    },
                  ),
                  onTap: () {
                    _selectTime(context);
                  },
                ),
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                child: RaisedButton(
                  padding: EdgeInsets.all(18),
                  onPressed: () {
                    if (widget.inputData['date'] != 'Choose Date' &&
                        widget.inputData['time'] != 'Choose Time') {
                      widget.executeOnValid();
                    }
                  },
                  child: Text(
                    'Next',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 16),
                  ),
                  color: (widget.inputData['date'] != 'Choose Date' &&
                          widget.inputData['time'] != 'Choose Time')
                      ? Colors.blueAccent
                      : Colors.blueAccent.withOpacity(0.7),
                ),
              )
            ],
          )
        ],
      ),
      padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
    );
  }

  validateForm() {}

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime.now(),
        lastDate: DateTime(2030));
    if (picked != null && picked != selectedDate)
      setState(() {
        String dateSlug =
            "${picked.day.toString().padLeft(2, '0')} ${getMonth(picked.month.toString().padLeft(2, '0'))} ${picked.year.toString()}";
        dateMenu.add(dateSlug);
        widget.inputData['date'] = dateSlug;
        selectedDate = picked;
      });
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());
    if (picked != null && picked != selectedTime)
      setState(() {
        timeMenu.add('${picked.hour}:${picked.minute}');
        widget.inputData['time'] = '${picked.hour}:${picked.minute}';
        selectedTime = picked;
      });
  }

  String getMonth(String m) {
    switch (m) {
      case "01":
        return "January";
      case "02":
        return "February";
      case "03":
        return "March";
      case "04":
        return "April";
      case "05":
        return "May";
      case "06":
        return "June";
      case "08":
        return "August";
      case "07":
        return "July";
      case "09":
        return "September";
      case "10":
        return "October";
      case "11":
        return "November";
      default:
        return "December";
    }
  }

  @override
  void dispose() {
    if (animationController != null) {
      animationController.dispose();
    }
    super.dispose();
  }
}
