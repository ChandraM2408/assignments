import 'package:flutter/material.dart';

import 'widgets/drop_down.dart';

class PersonalInformation extends StatefulWidget {
  final Function executeOnValid;
  final Map<String, String> inputData;

  PersonalInformation({Key key, this.executeOnValid, this.inputData}) : super(key: key);

  @override
  _PersonalInformationState createState() => _PersonalInformationState();
}

class _PersonalInformationState extends State<PersonalInformation> {
  bool isFormValid = false;
  List<String> goalMenu = [
    'Choose Option',
    'Lead team',
    'Oracle Certified',
  ];
  List<String> incomeMenu = [
    'Choose Option',
    '50,000',
    '1,00,000',
  ];
  List<String> expenseMenu = [
    'Choose Option',
    '25,000',
    '70,000',
  ];

  @override
  void initState() {
    validateForm();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 32),
      child: Column(
        children: [
          Expanded(
            child: Wrap(
              runSpacing: 16,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: Text(
                    'Personal Information',
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
                Text(
                  'Please fill in the information below and your goal for digital saving.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
                DropDown(
                  label: 'Goal for activation',
                  items: goalMenu,
                  selectedValue: widget.inputData['goal'],
                  onSelected: (value) {
                    setState(() {
                      widget.inputData['goal'] = value;
                    });
                    validateForm();
                  },
                ),
                DropDown(
                  label: 'Monthly Income',
                  items: incomeMenu,
                  selectedValue: widget.inputData['monthlyIncome'],
                  onSelected: (value) {
                    setState(() {
                      widget.inputData['monthlyIncome'] = value;
                    });
                    validateForm();
                  },
                ),
                DropDown(
                  label: 'Monthly Expense',
                  items: expenseMenu,
                  selectedValue: widget.inputData['monthlyExpense'],
                  onSelected: (value) {
                    setState(() {
                      widget.inputData['monthlyExpense'] = value;
                    });
                    validateForm();
                  },
                ),
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                child: RaisedButton(
                  padding: EdgeInsets.all(18),
                  onPressed: () {
                    if(isFormValid){
                      widget.executeOnValid();
                    }
                  },
                  child: Text(
                    'Next',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 16),
                  ),
                  color: isFormValid
                      ? Colors.blueAccent
                      : Colors.blueAccent.withOpacity(0.7),
                ),
              )
            ],
          )
        ],
      ),
      padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
    );
  }

  validateForm() {
    setState(() {
      isFormValid = widget.inputData['goal'] != goalMenu[0] &&
          widget.inputData['monthlyIncome'] != incomeMenu[0] &&
          widget.inputData['monthlyExpense'] != expenseMenu[0];
    });
  }
}
