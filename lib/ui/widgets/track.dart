import 'package:flutter/material.dart';

import 'progress_indicator.dart';

class Track extends StatefulWidget {

  final int selectedIndex;

  Track({Key key, this.selectedIndex}) : super(key: key);

  @override
  _TrackState createState() => _TrackState();
}

class _TrackState extends State<Track> {
  String email;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 12, bottom: 12),
      child: Center(
        child: ListView.separated(
          shrinkWrap: true,
          itemCount: 4,
          itemBuilder: (context, index) => BOFProgressIndicator(
            label: "${index + 1}",
            isEnabled: (index <= widget.selectedIndex),
          ),
          scrollDirection: Axis.horizontal,
          separatorBuilder: (context, index) => Row(
            children: [
              Container(
                height: 4,
                width: 48,
                color: Colors.black,
              )
            ],
          ),
        ),
      ),
      height: 72,
    );
  }
}
