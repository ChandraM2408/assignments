import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BOFProgressIndicator extends StatelessWidget {
  final String label;
  final bool isEnabled;

  const BOFProgressIndicator({Key key, this.label, this.isEnabled})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double size = 48.0;
    return Container(
      width: size,
      height: size,
      decoration: new BoxDecoration(
        color: this.isEnabled ? Colors.green : Colors.white,
        shape: BoxShape.circle,
        border: Border.all(color: Colors.black)
      ),
      child: new Center(
        child: Text("${this.label}", style: TextStyle(fontSize: 16),),
      ),
    );
  }
}
