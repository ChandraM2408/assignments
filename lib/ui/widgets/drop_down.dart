import 'package:flutter/material.dart';

class DropDown extends StatelessWidget {
  final String label;
  final List<String> items;
  final String selectedValue;
  final Function(String value) onSelected;
  final disableClick;

  const DropDown(
      {Key key,
      this.label,
      this.items,
      this.selectedValue,
      this.onSelected,
      this.disableClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          child: DropdownButtonFormField<String>(
            decoration: InputDecoration(
              enabledBorder: InputBorder.none,
              filled: true,
              labelText: label,
            ),
            isExpanded: true,
            value: selectedValue,
            hint: Text('', style: TextStyle(fontSize: 16)),
            items: items.map((String value) {
              return new DropdownMenuItem<String>(
                value: value,
                child: new Text(
                  value,
                  style: TextStyle(fontSize: 16),
                ),
              );
            }).toList(),
            onChanged: (selectedValue) {
              onSelected(selectedValue);
            },
          ),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(8)),
        ),
        dummyDropdownToDisable()
      ],
    );
  }

  Widget dummyDropdownToDisable() {
    return disableClick == true
        ? Opacity(
            opacity: 0,
            child: Container(
              child: DropdownButtonFormField<String>(
                decoration: InputDecoration(
                  enabledBorder: InputBorder.none,
                  filled: true,
                  labelText: label,
                ),
                isExpanded: true,
                hint: Text('', style: TextStyle(fontSize: 16)),
                items: [],
                onChanged: (selectedValue) {
                  onSelected(selectedValue);
                },
              ),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(8)),
            ),
          )
        : Container();
  }
}
