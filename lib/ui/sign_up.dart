import 'package:bank_of_the_future/ui/enter_password.dart';
import 'package:bank_of_the_future/ui/personal_information.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'schedule_call.dart';
import 'welcome.dart';
import 'widgets/track.dart';

class SignUp extends StatefulWidget {
  SignUp({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  var _selectedIndex = 0;
  String email;
  GlobalKey _trackV = GlobalKey();
  GlobalKey _appBarV = GlobalKey();
  var heightOfScrollView = 0.0;
  PageController _pageController = PageController();
  var scaffoldBackground;

  var inputs = {
    "email": "",
    "password": "",
    "goal": "Choose Option",
    "monthlyIncome": "Choose Option",
    "monthlyExpense": "Choose Option",
    "date": "Choose Date",
    "time": "Choose Time"
  };

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => setHeightOfScrollView());

    _pageController.addListener(() {
      setState(() {
        scaffoldBackground = _selectedIndex > 0 ? Colors.lightBlueAccent : null;
      });
    });
  }

  setHeightOfScrollView() {
    RenderBox _trackVBox = _trackV.currentContext.findRenderObject();
    RenderBox __appBarVBox = _appBarV.currentContext.findRenderObject();
    setState(() {
      heightOfScrollView = MediaQuery.of(context).size.height -
          _trackVBox.size.height -
          __appBarVBox.size.height;
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    return Scaffold(
      backgroundColor: scaffoldBackground,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor:
            _selectedIndex == 0 ? Colors.transparent : scaffoldBackground,
        leading: _selectedIndex == 0
            ? Container()
            : IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                ),
                onPressed: () {
                  onPrevious();
                }),
        key: _appBarV,
        title: Text(
          _selectedIndex == 0 ? '' : 'Create Account',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: Column(
        children: [
          Track(
            key: _trackV,
            selectedIndex: _selectedIndex - 1,
          ),
          Expanded(
            child: Center(
              child: SingleChildScrollView(
                child: Container(
                  height: heightOfScrollView,
                  child: PageView(
                    children: [
                      Welcome(
                        executeOnValid: () {
                          onNext();
                        },
                        inputData: inputs,
                      ),
                      EnterPassword(
                        inputData: inputs,
                        executeOnValid: () {
                          onNext();
                        },
                      ),
                      PersonalInformation(
                        inputData: inputs,
                        executeOnValid: () {
                          onNext();
                        },
                      ),
                      ScheduleCall(
                        inputData: inputs,
                        executeOnValid: () {
                          onNext();
                        },
                      ),
                      Container()
                    ],
                    controller: _pageController,
                  ),
                ),
              ),
            ),
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  onNext() {
    setState(() {
      _selectedIndex += 1;
    });
    _pageController.animateToPage(_pageController.page.toInt() + 1,
        duration: Duration(milliseconds: 300), curve: Curves.easeIn);
  }

  onPrevious() {
    setState(() {
      _selectedIndex -= 1;
    });
    _pageController.animateToPage(_pageController.page.toInt() - 1,
        duration: Duration(milliseconds: 300), curve: Curves.easeIn);
  }
}
